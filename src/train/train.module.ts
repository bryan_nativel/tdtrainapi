import { Module } from '@nestjs/common';
import { TrainService } from './train.service';
import { TrainController } from './train.controller';
import { ReservationService } from '../reservation/reservation.service';

@Module({
  controllers: [TrainController],
  providers: [TrainService, ReservationService],
  exports: [TrainService],
})
export class TrainModule {}
