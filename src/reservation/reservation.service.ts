import { Injectable, Res } from '@nestjs/common';
import { CreateReservationDto } from './dto/create-reservation.dto';
import { UpdateReservationDto } from './dto/update-reservation.dto';
import { mockReservation } from './mock/reservation..mock';
import { Reservation } from './entities/reservation.entity';
import { Train } from 'src/train/entities/train.entity';

@Injectable()
export class ReservationService {
  reservations: Reservation[] = mockReservation;

  create(reservation: CreateReservationDto) {
    // BookTrain different
    if (
      this.reservations.findIndex(
        (item) => item.bookNumber === reservation.bookNumber,
      ) == -1
    ) {
      this.reservations.push(
        new Reservation(
          reservation.bookNumber,
          reservation.currentTrain,
          reservation.numberPlaces,
        ),
      );

      return reservation;
    } else {
      return null;
    }
  }

  findAll() {
    return this.reservations;
  }

  findOne(id: string) {
    return this.reservations.find(
      (reservation) => reservation.bookNumber == id,
    );
  }

  update(id: string, updateReservationDto: UpdateReservationDto) {
    const reservation = this.reservations.find(
      (reservation) => reservation.bookNumber == id,
    );
    if (reservation) {
      const indexFind = this.reservations.indexOf(reservation);
      const updatedReservation = reservation;

      for (const reservationProp in updateReservationDto) {
        updatedReservation[reservationProp] =
          updateReservationDto[reservationProp];
      }
      this.reservations.splice(indexFind, 1, updatedReservation);
      return updatedReservation;
    } else {
      return null;
    }
  }

  remove(id: string) {
    const reservation = this.reservations.find(
      (reservation) => reservation.bookNumber == id,
    );
    if (reservation) {
      const indexFind = this.reservations.indexOf(reservation);
      this.reservations.splice(indexFind, 1);
      return `This action removed #${id} reservation`;
    } else {
      return 'reservation not found';
    }
  }

  computeAvailablePlaces(train: Train) {
    const places = this.reservations
      .filter((booking) => booking.currentTrain.numTrain === train.numTrain)
      .map((booking) => booking.numberPlaces)
      .reduce((totalPlaces, currentPlaces) => totalPlaces + currentPlaces, 0);
    /*const placesSmarter = this.reservations.reduce(
      (totalPlaces, currentBooking) => {
        if (currentBooking.currentTrain.numTrain === train.numTrain) {
          return totalPlaces + currentBooking.numberPlaces;
        }
      },
      0,
    );*/
    return train.place - places;
  }
}
