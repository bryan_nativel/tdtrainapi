import { Train } from 'src/train/entities/train.entity';

export class Reservation {
  private _bookNumber: string;
  private _currentTrain: Train;
  private _numberPlaces: number;

  constructor(bookNumber: string, currentTrain: Train, numberPlaces: number) {
    this._bookNumber = bookNumber;
    this._currentTrain = currentTrain;
    this._numberPlaces = numberPlaces;
  }
  get numberPlaces(): number {
    return this._numberPlaces;
  }

  set numberPlaces(value: number) {
    this._numberPlaces = value;
  }
  get currentTrain(): Train {
    return this._currentTrain;
  }

  set currentTrain(value: Train) {
    this._currentTrain = value;
  }
  get bookNumber(): string {
    return this._bookNumber;
  }

  set bookNumber(value: string) {
    this._bookNumber = value;
  }
}
