import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
} from '@nestjs/common';
import { ReservationService } from './reservation.service';
import { UpdateReservationDto } from './dto/update-reservation.dto';
import { Header } from '@nestjs/common';
import { CreateReservationDto } from './dto/create-reservation.dto';
import { TrainService } from '../train/train.service';
import { Reservation } from './entities/reservation.entity';

@Controller('reservation')
export class ReservationController {
  constructor(
    private readonly reservationService: ReservationService,
    private trainService: TrainService,
  ) {}

  @Post()
  @Header('Content-Type', 'application/json')
  create(@Body() createReservationDto: CreateReservationDto) {
    if (createReservationDto.numberPlaces <= 0) {
      return false;
    }
    const train = this.trainService.findOne(
      createReservationDto.currentTrain.numTrain,
    );
    if (!train) {
      return " Erreur : Train pour la réservation n'a pas été trouvé !! ";
    }
    const isNotFull =
      this.reservationService.computeAvailablePlaces(train) -
        createReservationDto.numberPlaces >=
      0;
    if (isNotFull) {
      return this.reservationService.create(createReservationDto)
        ? 'Réservation suuuuuuuucéééssssseeeee'
        : `Erreur durant la reservation ${createReservationDto.bookNumber}`;
    } else {
      return 'Pas assez de places disponibles';
    }
  }

  @Get()
  findAll() {
    return this.reservationService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.reservationService.findOne(id);
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateReservationDto: UpdateReservationDto,
  ) {
    //On recupére la reservation à modifier
    const train = this.trainService.findOne(
      updateReservationDto.currentTrain.numTrain,
    );
    if (!train) {
      return " Erreur : Train pour la réservation n'a pas été trouvé !! ";
    }
    const isNotFull =
      this.reservationService.computeAvailablePlaces(train) -
        updateReservationDto.numberPlaces >=
      0;

    if (isNotFull) {
      const reservation = this.reservationService.findOne(id);
      //Si la reservation n'est pas trouvé on la crée dans la else
      if (reservation) {
        // suppression de la reservation.
        this.reservationService.remove(id);
        //Création de la nouvel reservation
        this.reservationService.create(
          this.mergeReservation(reservation, updateReservationDto),
        );
      } else {
        this.reservationService.create(
          this.mergeReservation(reservation, updateReservationDto),
        );
      }
    } else {
      return 'Plus de place';
    }
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.reservationService.remove(id);
  }
  /* checkPlaceAvailable(reservation, trains = this.trains) {
    const numberPlace = trains.find(
      (train) => train.numTrain == reservation.currentTrain.numTrain,
    ).place;
    return numberPlace - reservation.numberPlaces >= 0;
  }*/

  mergeReservation(
    reservation: Reservation,
    nextReservation: UpdateReservationDto,
  ): CreateReservationDto {
    for (const reservationProp in nextReservation) {
      reservation[reservationProp] = nextReservation[reservationProp];
    }
    return reservation;
  }
}
