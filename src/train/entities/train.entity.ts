export class Train {
  private _villeDepart: string;
  private _numTrain: string;
  private _villeArriver: string;
  private _heureDepart: number;
  private _place: number;

  constructor(
    villeDepart: string,
    numTrain: string,
    villeArriver: string,
    heureDepart: number,
    place: number,
  ) {
    this._villeArriver = villeArriver;
    this._numTrain = numTrain;
    this._villeDepart = villeDepart;
    this._heureDepart = heureDepart;
    this._place = place;
  }

  get heureDepart(): number {
    return this._heureDepart;
  }

  set heureDepart(value: number) {
    this._heureDepart = value;
  }
  get villeArriver(): string {
    return this._villeArriver;
  }

  set villeArriver(value: string) {
    this._villeArriver = value;
  }
  get villeDepart(): string {
    return this._villeDepart;
  }

  set villeDepart(value: string) {
    this._villeDepart = value;
  }
  get numTrain(): string {
    return this._numTrain;
  }

  set numTrain(value: string) {
    this._numTrain = value;
  }
  get place(): number {
    return this._place;
  }

  set place(value: number) {
    this._place = value;
  }
}
