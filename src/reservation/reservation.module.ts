import { Module } from '@nestjs/common';
import { ReservationService } from './reservation.service';
import { ReservationController } from './reservation.controller';
import { TrainModule } from 'src/train/train.module';

@Module({
  controllers: [ReservationController],
  providers: [ReservationService],
  imports: [TrainModule],
})
export class ReservationModule {}
