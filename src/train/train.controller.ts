import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
} from '@nestjs/common';
import { TrainService } from './train.service';
import { CreateTrainDto } from './dto/create-train.dto';
import { UpdateTrainDto } from './dto/update-train.dto';
import { Train } from './entities/train.entity';
import { ReservationService } from '../reservation/reservation.service';

@Controller('train')
export class TrainController {
  constructor(
    private trainService: TrainService,
    private reservatioService: ReservationService,
  ) {}

  @Post()
  create(@Body() createTrainDto: CreateTrainDto) {
    return this.trainService.create(createTrainDto)
      ? 'train cRée'
      : "Erreur pour la création d'un train";
  }

  @Get()
  findAll() {
    return this.trainService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.trainService.findOne(id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateTrainDto: UpdateTrainDto) {
    const train = this.trainService.findOne(id);
    const reservations = this.reservatioService.reservations;
    const trains = reservations.filter(
      (reservation) => reservation.currentTrain.numTrain == id,
    );
    if (!trains) {
      if (train) {
        this.trainService.remove(id);
        return this.trainService.create(this.mergeTrain(train, updateTrainDto));
      } else {
        return this.trainService.create(
          this.mergeTrain(new Train(null, null, null, 0, 0), updateTrainDto),
        );
      }
    } else {
      return 'Le train est lié à une reservation';
    }
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    const reservations = this.reservatioService.reservations;
    const trains = reservations.filter(
      (reservation) => reservation.currentTrain.numTrain == id,
    );
    if (trains) {
      return 'Impossible de supprimer ce train est liée a une  ou des réservations ';
    } else {
      return this.trainService.remove(id);
    }
  }

  mergeTrain(train: Train, nextReservation: UpdateTrainDto): CreateTrainDto {
    for (const trainProp in nextReservation) {
      train[trainProp] = nextReservation[trainProp];
    }
    return train;
  }
}
