export class CreateTrainDto {
  villeArriver: string;
  numTrain: string;
  villeDepart: string;
  heureDepart: number;
  place: number;
}
