import { Injectable } from '@nestjs/common';
import { CreateTrainDto } from './dto/create-train.dto';
import { UpdateTrainDto } from './dto/update-train.dto';
import { Train } from './entities/train.entity';
import { mockTrain } from './mock/train..mock';
import { Reservation } from '../reservation/entities/reservation.entity';

@Injectable()
export class TrainService {
  bddTrain: Train[] = mockTrain;

  create(train: CreateTrainDto) {
    if (
      this.bddTrain.findIndex((item) => item.numTrain === train.numTrain) == -1
    ) {
      this.bddTrain.push(
        new Train(
          train.villeDepart,
          train.numTrain,
          train.villeArriver,
          train.heureDepart,
          train.place,
        ),
      );

      return train;
    } else {
      return null;
    }
  }

  findAll() {
    return this.bddTrain;
  }

  findOne(id: string) {
    return this.bddTrain.find((train) => train.numTrain == id);
  }

  update(id: number, updateTrainDto: UpdateTrainDto) {
    return `This action updates a #${id} train`;
  }

  remove(id: string) {
    const train = this.bddTrain.find((train) => train.numTrain == id);
    if (train) {
      const indexFind = this.bddTrain.indexOf(train);
      this.bddTrain.splice(indexFind, 1);
      return `This action removed #${id} train`;
    } else {
      return 'reservation not found';
    }
  }
}
