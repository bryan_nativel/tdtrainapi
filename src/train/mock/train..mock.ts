import { Train } from '../entities/train.entity';

export const mockTrain: Train[] = [
  new Train('Montpellier', 'train1', 'Paris', 1023, 5),
  new Train('lyon', 'train2', 'Montpellier', 121, 3),
  new Train('paris', 'train3', 'Lyon', 1430, 0),
];
