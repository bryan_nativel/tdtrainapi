import { PartialType } from '@nestjs/mapped-types/dist';
import { CreateTrainDto } from './create-train.dto';

export class UpdateTrainDto extends PartialType(CreateTrainDto) {}
