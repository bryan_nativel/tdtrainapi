import { Train } from '../../train/entities/train.entity';

export class CreateReservationDto {
  bookNumber: string;
  currentTrain: Train;
  numberPlaces: number;
}
