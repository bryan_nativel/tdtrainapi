import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TrainModule } from './train/train.module';
import { ReservationModule } from './reservation/reservation.module';

@Module({
  imports: [TrainModule, ReservationModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
